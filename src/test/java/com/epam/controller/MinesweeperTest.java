package com.epam.controller;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.*;

public class MinesweeperTest {
    private static final Logger LOGGER = LogManager.getLogger(MinesweeperTest.class);

    @BeforeSuite
    public void beforeSuite(){
        LOGGER.info("Hello! We are about to start running AllTest suite!\n");
    }

    @DataProvider(name = "values")
    public static Object[][] getData(){
        LOGGER.info("Using data provider to get parameters from CSV file!\n");
        return (Object[][])readFromCSV(MinesweeperTest.class.getResource("/values.csv")
                .getPath(), ":");
    }

    private static String[][] readFromCSV(String csvPath, String delimiter){
        List<String[]> data= new ArrayList<>();
        String line = "";
        try(BufferedReader br = new BufferedReader(new FileReader(csvPath))){
            LOGGER.info("Trying to read from CSV file!\n");
            while ((line = br.readLine()) != null) {
                data.add(line.split(delimiter));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        LOGGER.info("Data was read from CSV file!\n");
        return data.toArray(new String[data.size()][]);
    }

    @Test(dataProvider = "values")
    void testSetArray(String  M, String N, String p) {
        LOGGER.info("Get started to test setting right array!\n");
        Minesweeper minesweeper = new Minesweeper(Integer.parseInt(M), Integer.parseInt(N), Double.valueOf(p));
        Boolean[][] array = minesweeper.getArray();
        if(Integer.parseInt(M) <= 0 || Integer.parseInt(N) <= 0){
            assertNull(array);
            LOGGER.info("Array is null!\n");
        } else {
            assertNotNull(array);
            assertSame(Integer.parseInt(M), array.length);
            assertSame(Integer.parseInt(N), array[0].length);
            LOGGER.info("Array is not null with the right size!\n");
        }
    }

    @Test(dataProvider = "values")
    void testCreateModifiedArray(String  M, String N, String  p){
        LOGGER.info("Get started to test modified array!\n");
        Minesweeper minesweeper = new Minesweeper(Integer.parseInt(M), Integer.parseInt(N), Double.valueOf(p));
        String[] modifiedArray = minesweeper.getModifiedArray();
        String[] modifiedArrayWithNumbers = minesweeper.getModifiedArrayWithNumbers();
        if(Integer.parseInt(M) <= 0 || Integer.parseInt(N) <= 0){
            assertNull(modifiedArray);
            assertNull(modifiedArrayWithNumbers);
            LOGGER.info("Array is null!\n");
        } else {
            getAssertion(Integer.parseInt(M), Integer.parseInt(M), modifiedArray);
            getAssertion(Integer.parseInt(M), Integer.parseInt(N), modifiedArrayWithNumbers);
            LOGGER.info("All Arrays are not null with right size!\n");
        }
    }

    private void getAssertion(int M, int N, String[] modifiedArray) {
        assertNotNull(modifiedArray);
        assertEquals(M, modifiedArray.length);
        assertEquals(N * 2, modifiedArray[0].length());
    }

    @AfterSuite
    public void afterSuite(){
        LOGGER.info("You are out of suite. Bye!\n");
    }
}
