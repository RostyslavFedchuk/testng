package com.epam.controller;

import com.epam.controller.interfaces.PlateauFinder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;

import java.lang.reflect.Field;
import java.util.ResourceBundle;

import static org.mockito.Mockito.*;
import static org.testng.AssertJUnit.*;

public class PlateauFinderTest {
    private static final Logger LOGGER = LogManager.getLogger(PlateauFinderTest.class);

    @Test
    public void testSetArray() {
        LOGGER.info("Get started to test setting right array for PlateauFinderImpl!\n");
        Integer[] array = new Integer[]{1, 1, 1, 1, 0};
        PlateauFinderImpl sample = new PlateauFinderImpl(array);
        assertArrayEquals(array, sample.getArray());
        LOGGER.info("Array sets perfectly!\n");
    }

    @Test(invocationCount = 10)
    public void testSetRandomArray() {
        LOGGER.info("Get started to test setting random array for PlateauFinderImpl!\n");
        int size = 10;
        PlateauFinderImpl sample = new PlateauFinderImpl(size);
        assertEquals(size, sample.getArray().length);
        LOGGER.info("Random array sets perfectly!\n");
    }

    @Test
    public void testValues() {
        LOGGER.info("Testing values from PlateauFinderImpl!\n");
        ResourceBundle bundle = ResourceBundle.getBundle("config");
        int minValue = Integer.valueOf(bundle.getString("MIN_VALUE"));
        int maxValue = Integer.valueOf(bundle.getString("MAX_VALUE"));
        int size = 10;
        PlateauFinderImpl sample = new PlateauFinderImpl(size);
        Class sampleClass = sample.getClass();
        testField(minValue, sample, sampleClass, "MIN_VALUE");
        testField(maxValue, sample, sampleClass, "MAX_VALUE");
    }

    // Reflection!? Why not? :D
    private void testField(int minValue, PlateauFinderImpl sample, Class sampleClass, String min_value) {
        try {
            Field minVal = sampleClass.getDeclaredField(min_value);
            minVal.setAccessible(true);
            assertEquals(minValue, minVal.getInt(sample));
            LOGGER.info("Find Min_value correctly.\n");
        } catch (NoSuchFieldException | IllegalAccessException e) {
            LOGGER.info("NoSuchFieldException or IllegalAccessException\n");
            e.printStackTrace();
        }
    }

    @Test
    public void testFindLongestPlateau() {
        LOGGER.info("Testing main function! Trying to find longest plateau!\n");
        PlateauFinder sample = mock(PlateauFinder.class);
        Integer[] array = new Integer[]{0, 1, 1, 0};
        PlateauFinderImpl plateauFinder = new PlateauFinderImpl(array);
        doReturn("The longest plateau has length 2 and it is on interval: [1;2]\n")
                .when(sample).findLongestPlateau();
        assertEquals(plateauFinder.findLongestPlateau(), sample.findLongestPlateau());
        verify(sample).findLongestPlateau();
        LOGGER.info("PlateauFinderImpl find longest plateau correctly!\n");
    }
}
