package com.epam.controller.interfaces;

public interface PlateauFinder {
    String findLongestPlateau();
}
