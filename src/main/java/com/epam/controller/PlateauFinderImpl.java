package com.epam.controller;

import com.epam.controller.interfaces.PlateauFinder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.stream.Stream;

import static java.util.Objects.isNull;

public class PlateauFinderImpl implements PlateauFinder {
    private static Logger logger = LogManager.getLogger(PlateauFinderImpl.class);
    private static final int MIN_VALUE;
    private static final int MAX_VALUE;
    private static Random random;
    private Integer[] array;

    static {
        ResourceBundle bundle = ResourceBundle.getBundle("config");
        MIN_VALUE = Integer.valueOf(bundle.getString("MIN_VALUE"));
        MAX_VALUE = Integer.valueOf(bundle.getString("MAX_VALUE"));
        random = new Random();
    }

    public PlateauFinderImpl(int size) {
        setRandomArray(size);
    }

    public PlateauFinderImpl(Integer[] array) {
        setArray(array);
    }

    public void setArray(Integer[] array){
        this.array = array;
    }

    private void setRandomArray(int size) {
        if(size < 1){
            logger.info("Size cannot be less than 1!\n");
            return;
        }
        array = Stream.generate(() -> random.nextInt(MAX_VALUE - MIN_VALUE) + MIN_VALUE)
                .limit(size).toArray(n -> new Integer[size]);
    }

    public Integer[] getArray(){
        return array;
    }

    private Map<Integer, String> getPlateaus() {
        Map<Integer, String> plateaus = new LinkedHashMap<>();
        for (int i = 0; i < array.length; i++) {
            if (i > 0 && array[i - 1] >= array[i]) {
                continue;
            }
            checker(plateaus, i, i);
        }
        return plateaus;
    }

    private void checker(Map<Integer, String> plateaus, int i, int endIndex) {
        int length = 1;
        for (int j = i + 1; j < array.length; j++) {
            if (array[i].equals(array[j])) {
                length++;
                endIndex = j;
            } else {
                if(i > 0){
                    if (array[j - 1] > array[j]){
                        break;
                    } else {
                        length = 0;
                    }
                } else {
                    break;
                }
            }
        }
        if(length != 0){
            plateaus.put(length, "[" + i + ";" + endIndex + "]");
        }
    }

    public String findLongestPlateau() {
        String result = "";
        if(isNull(array)){
            logger.info("Your array is empty!\n");
            return result;
        }
        Optional<Map.Entry<Integer, String>> longest = getPlateaus().entrySet().stream()
                .max(Comparator.comparing(Map.Entry::getKey));
        if (longest.isPresent()) {
            result+="The longest plateau has length " + longest.get().getKey()
                    + " and it is on interval: " + longest.get().getValue() + "\n";
        } else {
            result+="There is no plateaus!";
        }
        return result;
    }

    public void printArray() {
        if(isNull(array)){
            logger.info("Your array is empty!\n");
        } else {
            logger.info("Your array:\n");
            Arrays.stream(array).forEach(s -> logger.info(s + " "));
            logger.info("\n");
        }
    }
}
